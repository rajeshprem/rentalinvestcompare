package com.nomoreboundaries.loancalc;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class BasicActivity extends Activity {

	EditText mEditPurchasePrice;
	EditText mEditDownPayment;
	EditText mEditMortgageRate;
	EditText mEditTerm;
	Button mCalculate;
	TextView mTextValMortgagePayment;
	TextView mTextValLoanAmount;

	AdView mAdView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_basic);

		mEditPurchasePrice = (EditText) findViewById(R.id.editPurchasePrice);
		mEditDownPayment = (EditText) findViewById(R.id.editDownPayment);
		mEditMortgageRate = (EditText) findViewById(R.id.editMortgageRate);
		mEditTerm = (EditText) findViewById(R.id.editTerm);

		mTextValMortgagePayment = (TextView) findViewById(R.id.textValMortgagePayment);
		mTextValLoanAmount = (TextView) findViewById(R.id.textValLoanAmount);

		mAdView = (AdView)this.findViewById(R.id.adView);
		mAdView.setAdListener(new MyAdListener());

		AdRequest adRequest = new AdRequest();
		adRequest.addKeyword("loan calculator");
		adRequest.addKeyword("mortgage calculator");
		adRequest.addKeyword("investment calculator");
		adRequest.addKeyword("rental calculator");
		mAdView.loadAd(adRequest);

	}
	
	 @Override
	  public void onDestroy() {
	    if (mAdView != null) {
	      mAdView.destroy();
	    }

	    super.onDestroy();
	  }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private class MyAdListener implements AdListener {

		@Override
		public void onFailedToReceiveAd(Ad ad, ErrorCode errorCode) {
		}

		@Override
		public void onReceiveAd(Ad ad) {
		}

		@Override
		public void onDismissScreen(Ad arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLeaveApplication(Ad arg0) {
		}

		@Override
		public void onPresentScreen(Ad arg0) {
			// TODO Auto-generated method stub

		}
	}

	public double getPurchasePrice(EditText mEditPurchasePrice) {
		String sPurchasePrice = mEditPurchasePrice.getText().toString();
		double purchasePrice = 0;
		if (!sPurchasePrice.isEmpty()) {
			purchasePrice = Double.parseDouble(mEditPurchasePrice.getText()
					.toString());
		} else {
			Toast.makeText(getApplicationContext(), "Enter Purchase Price",
					Toast.LENGTH_SHORT).show();
		}
		return purchasePrice;
	}

	/**
	 * Down Payment in Percentage
	 * 
	 * @param mEditPurchasePrice
	 * @return
	 */
	public double getDownPayment(EditText mEditPurchasePrice) {
		String sDownPayment = mEditPurchasePrice.getText().toString();
		double downPayment = 0;
		if (!sDownPayment.isEmpty()) {
			downPayment = Double.parseDouble(mEditDownPayment.getText()
					.toString());
		} else {
			Toast.makeText(getApplicationContext(), "Enter Down Payment",
					Toast.LENGTH_SHORT).show();
		}
		return downPayment;
	}

	public double getRate(EditText mEditMortgageRate) {
		double rate = 0;
		String sRate = this.mEditMortgageRate.getText().toString();
		if (!sRate.isEmpty()) {
			rate = Double.parseDouble(sRate);
		} else {
			Toast.makeText(getApplicationContext(), "Enter Mortgage Rate",
					Toast.LENGTH_SHORT).show();
		}
		return rate;
	}

	public int getTerm(EditText mEditTerm) {
		int term = 0;
		String sTerm = this.mEditTerm.getText().toString();

		if (!sTerm.isEmpty()) {
			term = Integer.parseInt(sTerm);
		} else {
			Toast.makeText(getApplicationContext(), "Enter Term (in Months)",
					Toast.LENGTH_SHORT).show();
		}
		return term;
	}

	/**
	 * "Calculate" Button's onClickListener method
	 * 
	 * @param view
	 */
	public void calculate(View view) {

		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mEditTerm.getWindowToken(), 0);

		double purchasePrice = getPurchasePrice(this.mEditPurchasePrice);
		double downPayment = getDownPayment(this.mEditDownPayment); // in
																	// percentage
		double loanAmount;
		DecimalFormat df = new DecimalFormat("$###,###.00");

		if (purchasePrice != 0 && downPayment != 0) {
			downPayment = getDownPayment(this.mEditDownPayment) * purchasePrice
					/ 100; // in absolute amount
			loanAmount = purchasePrice - downPayment;
			double rate = getRate(this.mEditMortgageRate) / 100;
			int term = getTerm(this.mEditTerm);
			System.out.println(term);

			if (rate != 0 && term != 0) {
				double payment = calculatePayment(loanAmount, rate, term);
				this.mTextValMortgagePayment.setText(df.format(payment));
				this.mTextValLoanAmount.setText(df.format(loanAmount));
			} else {
				Toast.makeText(getApplicationContext(), "Rate & Term problem",
						Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(getApplicationContext(), "PP & DP problem",
					Toast.LENGTH_SHORT).show();
		}
	}

	public double calculatePayment(double loanAmount, double rate, int term) {

		double monthlyRate = (rate / 12);
		double discountFactor = (Math.pow((1 + monthlyRate), term) - 1)
				/ (monthlyRate * Math.pow((1 + monthlyRate), term));
		double monthlyPayment = loanAmount / discountFactor;
		return monthlyPayment;
	}

}
