package com.nomoreboundaries.loancalc;

import java.text.DecimalFormat;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.google.ads.AdRequest.ErrorCode;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AdvancedActivity extends Activity {

	EditText mEditPurchasePrice;
	EditText mEditDownPayment;
	EditText mEditMortgageRate;
	EditText mEditTerm;
	EditText mEditPropertyTax;
	EditText mEditPMI;
	EditText mEditHOA;
	Button mCalculate;
	TextView mTextValPandI;
	TextView mTextValMonthlyPayment;
	
	AdView mAdView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_advanced);
		
		mEditPurchasePrice   = (EditText)findViewById(R.id.editAdvPurchasePrice);
		mEditDownPayment = (EditText)findViewById(R.id.editAdvDownPayment);
		mEditMortgageRate = (EditText)findViewById(R.id.editAdvMortgageRate);
		mEditTerm = (EditText)findViewById(R.id.editAdvTerm);
		mEditPropertyTax = (EditText)findViewById(R.id.editAdvPropertyTax);
		mEditPMI = (EditText)findViewById(R.id.editAdvPMI);
		mEditHOA = (EditText)findViewById(R.id.editAdvHOA);
		
		mTextValPandI = (TextView)findViewById(R.id.textValPandI);
		mTextValMonthlyPayment = (TextView)findViewById(R.id.textValTotalPayment);
		
		mAdView = (AdView)this.findViewById(R.id.adView);
		mAdView.setAdListener(new MyAdListener());

		AdRequest adRequest = new AdRequest();
		adRequest.addKeyword("loan calculator");
		adRequest.addKeyword("mortgage calculator");
		adRequest.addKeyword("investment calculator");
		adRequest.addKeyword("rental calculator");
		mAdView.loadAd(adRequest);
	}
	
	@Override
	public void onDestroy() {
		if (mAdView != null) {
			mAdView.destroy();
		}

		super.onDestroy();
	}

	private class MyAdListener implements AdListener {

		@Override
		public void onFailedToReceiveAd(Ad ad, ErrorCode errorCode) {
		}

		@Override
		public void onReceiveAd(Ad ad) {
		}

		@Override
		public void onDismissScreen(Ad arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLeaveApplication(Ad arg0) {
		}

		@Override
		public void onPresentScreen(Ad arg0) {
			// TODO Auto-generated method stub

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.advanced, menu);
		return true;
	}
	
	public double getPurchasePrice(EditText mEditPurchasePrice)
	{
		String sPurchasePrice = mEditPurchasePrice.getText().toString();
		double purchasePrice = 0;
		if (!sPurchasePrice.isEmpty()) {
			purchasePrice = Double.parseDouble(mEditPurchasePrice.getText().toString());
		} else { 
			Toast.makeText(getApplicationContext(), "Enter Purchase Price", Toast.LENGTH_SHORT).show();
		}
		return purchasePrice;
	}
	
	public double getDownPayment(EditText mEditPurchasePrice)
	{
		String sDownPayment = mEditPurchasePrice.getText().toString();
		double downPayment = 0;
		if (!sDownPayment.isEmpty()) {
		 downPayment =  Double.parseDouble(mEditDownPayment.getText().toString());
		} else { 
			Toast.makeText(getApplicationContext(), "Enter Down Payment", Toast.LENGTH_SHORT).show();
		}
		return downPayment;
	}
	
	public double getRate(EditText mEditMortgageRate) {
		double rate = 0;
		String sRate = this.mEditMortgageRate.getText().toString();
		if (!sRate.isEmpty()) {
			rate = Double.parseDouble(sRate);
		} else {
			Toast.makeText(getApplicationContext(), "Enter Mortgage Rate", Toast.LENGTH_SHORT).show();
		}
		return rate;
	}
	
	public int getTerm(EditText mEditTerm) {
		int term = 0;
		String sTerm = this.mEditTerm.getText().toString();

		if (!sTerm.isEmpty()) {
			term = Integer.parseInt(sTerm);
		} else {
			Toast.makeText(getApplicationContext(), "Enter Term (in Months)", Toast.LENGTH_SHORT).show();
		}
		return term;
	}
	
	public double getPropertyTax(EditText mEditPropertyTax) {
		double pTax = 0;
		String sRate = this.mEditPropertyTax.getText().toString();
		if (!sRate.isEmpty()) {
			pTax = Double.parseDouble(sRate);
		} 
		return pTax;
	}
	
	public double getPMI(EditText mEditPMI) {
		double pmi = 0;
		String sRate = this.mEditPMI.getText().toString();
		if (!sRate.isEmpty()) {
			pmi = Double.parseDouble(sRate);
		} 
		return pmi;
	}
	
	public double getHOA(EditText mEditHOA) {
		double hoa = 0;
		String sRate = this.mEditHOA.getText().toString();
		if (!sRate.isEmpty()) {
			hoa = Double.parseDouble(sRate);
		} 
		return hoa;
	}
	
	public double calculatePayment(double loanAmount, double rate, int term) {

		double monthlyRate = (rate / 12);
		double discountFactor = (Math.pow((1 + monthlyRate), term) - 1)
				/ (monthlyRate * Math.pow((1 + monthlyRate), term));
		double pAndI = loanAmount / discountFactor;
		return pAndI;
	}
	
	/**
	 * 1.25 * 375000 / 100 
	 * @param purchasePrice
	 * @param propertyTaxRate
	 * @return
	 */
	public double calculatePropertyTax(double purchasePrice, double propertyTaxRate)
	{
		return (propertyTaxRate * purchasePrice) / 100;
	}
	
	public double calculatePMI(double loanAmount, double pmiRate)
	{
		return (pmiRate * loanAmount) / 100;
	}
	
	
	public void calculate(View view) {

		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mEditTerm.getWindowToken(), 0);

		double purchasePrice = getPurchasePrice(this.mEditPurchasePrice);
		double downPayment = getDownPayment(this.mEditDownPayment); 
		double loanAmount;
		double propertyTax = calculatePropertyTax(purchasePrice, getPropertyTax(this.mEditPropertyTax)) / 12;
		double pmi;
		double hoa = getHOA(this.mEditHOA);
		
		DecimalFormat df = new DecimalFormat("$###,###.00");

		if (purchasePrice != 0 && downPayment != 0) {
			downPayment = getDownPayment(this.mEditDownPayment) * purchasePrice
					/ 100; // in absolute amount
			loanAmount = purchasePrice - downPayment;
			double rate = getRate(this.mEditMortgageRate) / 100;
			int term = getTerm(this.mEditTerm);
			pmi = calculatePMI(loanAmount, getPMI(this.mEditPMI)) / 12;
			System.out.println(term);

			if (rate != 0 && term != 0) {
				double pAndI = calculatePayment(loanAmount, rate, term);
				this.mTextValPandI.setText(df.format(pAndI));
				double totalPayment = pAndI + propertyTax + pmi + hoa;
				this.mTextValMonthlyPayment.setText(df.format(totalPayment));
			} else {
				Toast.makeText(getApplicationContext(), "Rate & Term problem",
						Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(getApplicationContext(), "PP & DP problem",
					Toast.LENGTH_SHORT).show();
		}
	}

	
	
	

}
