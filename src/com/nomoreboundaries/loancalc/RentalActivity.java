package com.nomoreboundaries.loancalc;

import java.text.DecimalFormat;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.google.ads.AdRequest.ErrorCode;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RentalActivity extends Activity {
	
	EditText mEditPurchasePrice;
	EditText mEditDownPayment;
	EditText mEditMortgageRate;
	EditText mEditTerm;
	EditText mEditPropertyTax;
	EditText mEditHOA;
	EditText mEditClosing;
	EditText mEditInsurance;
	EditText mEditExpectedRent;
	EditText mEditMaintenance;
	EditText mEditOtherExpenses;
	EditText mEditOtherIncome;

	Button mCalculate;
	
	TextView mTextValMonthlyPayment;
	TextView mTextValCashFlow;
	TextView mTextValNOI;
	TextView mTextValReturnOfEquityYearOne;
	TextView mTextValGRM;
	TextView mTextBreakEven;
	
	AdView mAdView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rental);
		
		mEditPurchasePrice   = (EditText)findViewById(R.id.editRentalPurchasePrice);
		mEditDownPayment = (EditText)findViewById(R.id.editRentalDownPayment);
		mEditMortgageRate = (EditText)findViewById(R.id.editRentalRate);
		mEditTerm = (EditText)findViewById(R.id.editRentalTerm);
		mEditPropertyTax = (EditText)findViewById(R.id.editRentalPropertyTax);
		mEditHOA = (EditText)findViewById(R.id.editRentalHOA);
		mEditClosing = (EditText)findViewById(R.id.editRentalClosingCost);
		mEditInsurance = (EditText)findViewById(R.id.editRentalInsurance);
		mEditExpectedRent = (EditText)findViewById(R.id.editRentalExpectedRent);
		mEditMaintenance = (EditText)findViewById(R.id.editRentalMaintenance);
		mEditOtherExpenses = (EditText)findViewById(R.id.editRentalOtherExpenses);
		mEditOtherIncome = (EditText)findViewById(R.id.editRentalOtherIncome);
		
		mTextValMonthlyPayment = (TextView)findViewById(R.id.textValRentalMonthlyPayment);
		mTextValCashFlow = (TextView)findViewById(R.id.txtValCashFlow);
		mTextValNOI = (TextView)findViewById(R.id.txtValNOI);
		mTextValReturnOfEquityYearOne = (TextView)findViewById(R.id.txtValReturnOnEquityYearOne);
		mTextValGRM = (TextView)findViewById(R.id.txtValGRM);
		mTextBreakEven = (TextView)findViewById(R.id.txtValBreakEven);
		
		mAdView = (AdView)this.findViewById(R.id.adView);
		mAdView.setAdListener(new MyAdListener());

		AdRequest adRequest = new AdRequest();
		adRequest.addKeyword("loan calculator");
		adRequest.addKeyword("mortgage calculator");
		adRequest.addKeyword("investment calculator");
		adRequest.addKeyword("rental calculator");
		mAdView.loadAd(adRequest);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.rental, menu);
		return true;
	}
	
	@Override
	public void onDestroy() {
		if (mAdView != null) {
			mAdView.destroy();
		}

		super.onDestroy();
	}

	private class MyAdListener implements AdListener {

		@Override
		public void onFailedToReceiveAd(Ad ad, ErrorCode errorCode) {
		}

		@Override
		public void onReceiveAd(Ad ad) {
		}

		@Override
		public void onDismissScreen(Ad arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLeaveApplication(Ad arg0) {
		}

		@Override
		public void onPresentScreen(Ad arg0) {
			// TODO Auto-generated method stub

		}
	}
	
	public double getPurchasePrice(EditText mEditPurchasePrice)
	{
		String sPurchasePrice = mEditPurchasePrice.getText().toString();
		double purchasePrice = 0;
		if (!sPurchasePrice.isEmpty()) {
			purchasePrice = Double.parseDouble(mEditPurchasePrice.getText().toString());
		} else { 
			Toast.makeText(getApplicationContext(), "Enter Purchase Price", Toast.LENGTH_SHORT).show();
		}
		return purchasePrice;
	}
	
	public double getDownPayment(EditText mEditPurchasePrice)
	{
		String sDownPayment = mEditPurchasePrice.getText().toString();
		double downPayment = 0;
		if (!sDownPayment.isEmpty()) {
		 downPayment =  Double.parseDouble(mEditDownPayment.getText().toString());
		} else { 
			Toast.makeText(getApplicationContext(), "Enter Down Payment", Toast.LENGTH_SHORT).show();
		}
		return downPayment;
	}
	
	public double getRate(EditText mEditMortgageRate) {
		double rate = 0;
		String sRate = this.mEditMortgageRate.getText().toString();
		if (!sRate.isEmpty()) {
			rate = Double.parseDouble(sRate);
		} else {
			Toast.makeText(getApplicationContext(), "Enter Mortgage Rate", Toast.LENGTH_SHORT).show();
		}
		return rate;
	}
	
	public int getTerm(EditText mEditTerm) {
		int term = 0;
		String sTerm = this.mEditTerm.getText().toString();

		if (!sTerm.isEmpty()) {
			term = Integer.parseInt(sTerm);
		} else {
			Toast.makeText(getApplicationContext(), "Enter Term (in Months)", Toast.LENGTH_SHORT).show();
		}
		return term;
	}
	
	public double getPropertyTax(EditText mEditPropertyTax) {
		double pTax = 0;
		String sRate = this.mEditPropertyTax.getText().toString();
		if (!sRate.isEmpty()) {
			pTax = Double.parseDouble(sRate);
		} 
		return pTax;
	}
	
	public double getClosingCosts(EditText mEditClosing) {
		double pmi = 0;
		String sClosing = this.mEditClosing.getText().toString();
		if (!sClosing.isEmpty()) {
			pmi = Double.parseDouble(sClosing);
		} 
		return pmi;
	}
	
	public double getHOA(EditText mEditHOA) {
		double hoa = 0;
		String sRate = this.mEditHOA.getText().toString();
		if (!sRate.isEmpty()) {
			hoa = Double.parseDouble(sRate);
		} 
		return hoa;
	}
	
	public double getInsurance(EditText mEditInsurance) {
		String val = this.mEditInsurance.getText().toString();
		return val.isEmpty() ?  0 : Double.parseDouble(val);
	}
	
	public double getExpectedRent(EditText mEditExpectedRent) {
		String val = this.mEditExpectedRent.getText().toString();
		return val.isEmpty() ?  0 : Double.parseDouble(val);
	}
	
	public double getMaintenanceCosts(EditText mEditMaintenance) {
		String val = this.mEditMaintenance.getText().toString();
		return val.isEmpty() ?  0 : Double.parseDouble(val);
	}
	
	public double getOtherExpenses(EditText mEditOtherExpenses) {
		String val = this.mEditOtherExpenses.getText().toString();
		return val.isEmpty() ?  0 : Double.parseDouble(val);
	}
	
	public double getOtherIncome(EditText mEditOtherIncome) {
		String val = this.mEditOtherIncome.getText().toString();
		return val.isEmpty() ?  0 : Double.parseDouble(val);
	}
	
	public double calculatePayment(double loanAmount, double rate, int term) {

		double monthlyRate = (rate / 12);
		double discountFactor = (Math.pow((1 + monthlyRate), term) - 1)
				/ (monthlyRate * Math.pow((1 + monthlyRate), term));
		double pAndI = loanAmount / discountFactor;
		return pAndI;
	}
	
	public double calculatePropertyTax(double purchasePrice, double propertyTaxRate)
	{
		return (propertyTaxRate * purchasePrice) / 100;
	}
	
/*	public double calculateLoanAmount(double purchasePrice, double downPaymentPercentage)
	{
		double downPayment = downPaymentPercentage * purchasePrice /100;
		return purchasePrice - downPayment;
	}
	*/
	
	public double calculateGrossOperatingIncome(double expectedRent)
	{
		return expectedRent * 12;
	}
	
	public double calculateOperatingExpenses(double maintenanceCosts, double operatingExpenses, double operatingIncome)
	{
		return (maintenanceCosts + operatingExpenses - operatingIncome) * 12;
	}
	
	public void calculate(View view) {

		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mEditTerm.getWindowToken(), 0);

		double purchasePrice = getPurchasePrice(this.mEditPurchasePrice);
		double downPayment = getDownPayment(this.mEditDownPayment); 
		double loanAmount;
		double propertyTax = calculatePropertyTax(purchasePrice, getPropertyTax(this.mEditPropertyTax)) / 12;
		double hoa = getHOA(this.mEditHOA);
		double closingCosts = getClosingCosts(this.mEditClosing);
		double monthlyInsurance = getInsurance(this.mEditInsurance) / 12;
		double pAndI = 0;
		
		DecimalFormat df = new DecimalFormat("$###,###.00");
		DecimalFormat pf = new DecimalFormat("##.00%");
		DecimalFormat gf = new DecimalFormat("##.00");

		if (purchasePrice != 0 && downPayment != 0) {
			downPayment = getDownPayment(this.mEditDownPayment) * purchasePrice	/ 100; 
			loanAmount = purchasePrice - downPayment;
			double rate = getRate(this.mEditMortgageRate) / 100;
			int term = getTerm(this.mEditTerm);
			System.out.println(term);
			if (rate != 0 && term != 0) {
				pAndI = calculatePayment(loanAmount, rate, term);
				double totalMonthlyPayment = pAndI + propertyTax  + hoa + monthlyInsurance;
				this.mTextValMonthlyPayment.setText(df.format(totalMonthlyPayment));
			} 
		} 
		
		//NOI
		double goi = calculateGrossOperatingIncome(getExpectedRent(this.mEditExpectedRent));
		double operatingExpenses = calculateOperatingExpenses(getMaintenanceCosts(this.mEditMaintenance), getOtherExpenses(this.mEditOtherExpenses), getOtherIncome(this.mEditOtherIncome) );
		double noi = goi - operatingExpenses;  //per year
		this.mTextValNOI.setText(df.format(noi));
		
		//GRM & Break Even
		double debtService = pAndI * 12;  // per year
		double breakEven = 0;
		double grm = 0;
		if (goi != 0) {
			breakEven = ((debtService + operatingExpenses) / goi);
			grm = purchasePrice / goi;
		}

		this.mTextBreakEven.setText(pf.format(breakEven));
		this.mTextValGRM.setText(gf.format(grm));

		
		//Cash flow
		double maintenanceCosts = getMaintenanceCosts(this.mEditMaintenance) * 12; //per year
		double otherExpenses = getOtherExpenses(this.mEditOtherExpenses) * 12; //per year
		double cfbt = noi - debtService - maintenanceCosts - otherExpenses ;
		this.mTextValCashFlow.setText(df.format(cfbt));
		
		//ROR Y-1
		double cashIn = downPayment + closingCosts;
		double cfat = cfbt - 7500.00;
		double ror = 0;
		
		if (cashIn != 0 && cfbt != 0) {
			ror = cfat / cashIn;
		}
		this.mTextValReturnOfEquityYearOne.setText(pf.format(ror));
		
	}
	
}

